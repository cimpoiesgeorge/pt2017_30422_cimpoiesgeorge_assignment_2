
public class Task {
	
		private int servingTime;
		private int arrivalTime;
		private int cId;


		public Task(int servingTime, int arrivalTime, int cId) { 
			this.setServingTime(servingTime);
			this.setArrivalTime(arrivalTime);
			this.setcId(cId);
		}

		// timp servire
		public int getServingTime() {
			return servingTime;
		}

		public void setServingTime(int servingTime) {
			this.servingTime = servingTime;
		}

		// timp sosire
		public int getArrivalTime() {
			return arrivalTime;
		}

		public void setArrivalTime(int arrivalTime) {
			this.arrivalTime = arrivalTime;
		}

		// client ID
		public int getcId() {
			return cId;
		}

		public void setcId(int cId) {
			this.cId = cId;
		}

}
