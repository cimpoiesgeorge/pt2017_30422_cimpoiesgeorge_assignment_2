
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.JProgressBar;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPanel;

public class Graphic extends JPanel {

		
		public Graphic() {
			setBackground(Color.LIGHT_GRAY);
		}

		public void updateThis() {
			this.repaint();
		}

		@Override
		protected void paintComponent(Graphics g) {
			super.paintComponent(g);

			int x = 60;

			for (int i = 0; i < Scheduler.servers.length; i++) {
				// desenez "casa de marcat"
				int y = 0;
				drawQueue(g, x, y);

				// Desenez clientii in functie de ServiceTime
				for (int j = 0; j < Scheduler.servers[i].size(); j++) {
					y += 30;
					
					int id = Scheduler.servers[i].getCurrentClient(j).getcId() + 1;

					if (Scheduler.servers[i].getCurrentClient(j).getServingTime() < 5) {
						drawCustomer(g, x + 20, y, Color.yellow, id);
					} else {
						if (Scheduler.servers[i].getCurrentClient(j).getServingTime() < 10) {
							drawCustomer(g, x + 20, y, Color.orange, id);
						} else {
							if (Scheduler.servers[i].getCurrentClient(j)
									.getServingTime() < 15) {
								drawCustomer(g, x + 20, y, Color.magenta, id);
							} else {
								drawCustomer(g, x + 20, y, Color.red, id);
							}
						}
					}

				}

				x += 100;
			}

		}

		
		private void drawQueue(Graphics g, int x, int y) {
			g.setColor(Color.YELLOW);
			g.fillRect(x, y, 90, 20);
			g.setColor(Color.black);
			g.drawString("Register", x + 15, y + 15);
			this.repaint();
		}

		private void drawCustomer(Graphics g, int x, int y, Color c, int clientId) {
			g.setColor(c);
			g.fillOval(x, y, 50, 15);
			g.setColor(Color.black);
			g.drawString(Integer.toString(clientId), x + 20, y + 12);
			this.repaint();
		}
		
	}

