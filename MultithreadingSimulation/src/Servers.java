
import java.util.LinkedList;



public class Servers extends Thread {

	//lista cu clienti in server
	private LinkedList<Task> server = new LinkedList<>();
																	
	private int nullTime;

	public void run() {
		while (true) {
			if (!server.isEmpty()) {
				try {
					sleep(server.getFirst().getArrivalTime() * 1000);
					Scheduler.appendLog("Task  "
							+ (server.getFirst().getcId() + 1)
							+ "       leave at the moment:   "
							+ (Scheduler.getRealTime() - 1) + "\n");
				} catch (InterruptedException e) {
					e.printStackTrace();
				} finally {
					Scheduler.addWaitingTime(Scheduler.getRealTime()
							- server.getFirst().getArrivalTime()
							- server.getFirst().getServingTime());
					server.removeFirst();

				}
			} else {
				nullTime++;
				try {
					sleep(1000);

				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	// adauga client la server
	public void addTask(Task a) {
		server.add(a);
	}

	// servire client
	public Task servingTask() {
		return server.removeFirst();
	}

	// lungimea cozii
	public int size() {
		return server.size();
	}

	// timp de asteptare
	public int getWaitingTime() {
		return nullTime;
	}

	public void setWaitingTime(int waitingTime) {
		this.nullTime = waitingTime;
	}

	public boolean isEmpty() {
		return server.isEmpty();
	}

	public Task getCurrentClient(int i) {
		return server.get(i);
	}

}