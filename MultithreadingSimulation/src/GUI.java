
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;



import java.awt.Color;



public class GUI {

		private JFrame frame;
		public static JTextArea logTxt = new JTextArea();
		public static JTextField textTime = new JTextField();

		public static int getScreenWidth() {
			return java.awt.GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds().width;
		}

		public static int getScreenHeight() {
			return java.awt.GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds().height;
		}

		// Proprietati
		private int frameWidth = 800;
		private int frameHeight = 600;
		private JTextField txtMinArrival;
		private JTextField txtMaxArrival;
		private JTextField txtMinServing;
		private JTextField txtMaxServing;
		private JTextField txtQueueNr;
		private JTextField txtSimTime;

		
		/**
		 * Launch the application.
		 */
		public static void main(String[] args) {

			GUI window = new GUI();
			window.frame.setVisible(true);

		}

		/**
		 * Create the application.
		 */
		public GUI() {
			initialize();
		}

		/**
		 * Initialize the contents of the frame.
		 */
		private void initialize() {

			// setari generale
			frame = new JFrame();
			
			// pun frame-ul  pe mijlocul ecranului
			frame.setBounds(getScreenWidth() / 2 - frameWidth, getScreenHeight() / 2 - frameHeight / 2, frameWidth, frameHeight); 
			
			
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.setResizable(false);
			frame.getContentPane().setLayout(null);
			frame.setTitle("QUEUES SIMULATION-> homework no.3");

			// TextField + Buton simulare
			addTextSim();

			// Log
			addLog();

		}

		
		private void addTextSim() {
			
			JLabel lblMinServingTime = new JLabel("Introduce Serving Time:  min:");
			lblMinServingTime.setFont(new Font("Times New Roman", Font.BOLD, 15));
			lblMinServingTime.setBounds(30, 30, 200, 20);
			frame.getContentPane().add(lblMinServingTime);

			JLabel lblMaxServingTime = new JLabel("max:");
			lblMaxServingTime.setFont(new Font("Times New Roman", Font.BOLD, 15));
			lblMaxServingTime.setBounds(390, 30, 200, 20);
			frame.getContentPane().add(lblMaxServingTime);
			
			JLabel lblNewLabel_1 = new JLabel("Introduce Arrival Time:   min:");
			lblNewLabel_1.setFont(new Font("Times New Roman", Font.BOLD, 15));
			lblNewLabel_1.setBounds(30, 70, 200, 20);
			frame.getContentPane().add(lblNewLabel_1);

			JLabel lblMaxArrivalTime = new JLabel("max:");
			lblMaxArrivalTime.setFont(new Font("Times New Roman", Font.BOLD, 15));
			lblMaxArrivalTime.setBounds(390, 70, 200, 20);
			frame.getContentPane().add(lblMaxArrivalTime);

			JLabel lblSimulationTime = new JLabel("Simulation Time:");
			lblSimulationTime.setFont(new Font("Times New Roman", Font.BOLD, 15));
			lblSimulationTime.setBounds(45, 140, 127, 20);
			frame.getContentPane().add(lblSimulationTime);

			JLabel lblQueueNumber = new JLabel("Queues:");
			lblQueueNumber.setFont(new Font("Times New Roman", Font.BOLD, 15));
			lblQueueNumber.setBounds(330, 140, 127, 20);
			frame.getContentPane().add(lblQueueNumber);


			txtMinArrival = new JTextField();
			txtMinArrival.setBounds(290, 70, 50, 20);
			frame.getContentPane().add(txtMinArrival);
			txtMinArrival.setColumns(15);

			txtMaxArrival = new JTextField();
			txtMaxArrival.setColumns(10);
			txtMaxArrival.setBounds(435, 70, 50, 20);
			frame.getContentPane().add(txtMaxArrival);

			txtMinServing = new JTextField();
			txtMinServing.setColumns(10);
			txtMinServing.setBounds(290, 30, 50, 20);
			frame.getContentPane().add(txtMinServing);

			txtMaxServing = new JTextField();
			txtMaxServing.setColumns(10);
			txtMaxServing.setBounds(435, 30, 50, 20);
			frame.getContentPane().add(txtMaxServing);

			txtQueueNr = new JTextField();
			txtQueueNr.setColumns(10);
			txtQueueNr.setBounds(395, 140, 66, 20);
			frame.getContentPane().add(txtQueueNr);

			txtSimTime = new JTextField();
			txtSimTime.setColumns(10);
			txtSimTime.setBounds(170, 140, 50, 20);
			frame.getContentPane().add(txtSimTime);

			JButton btnStartSim = new JButton("START");
			btnStartSim.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					verifyAndCreateQueueManager();
				}
			});
			btnStartSim.setFont(new Font("Times New Roman", Font.BOLD, 17));
			btnStartSim.setBounds(580, 50, 166, 65);
			frame.getContentPane().add(btnStartSim);
		}

		/**
		 * Adaug pe frame Time + Log
		 */
		private void addLog() {
			JLabel lblNewLabel = new JLabel("Moment:");
			lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
			lblNewLabel.setForeground(Color.RED);
			lblNewLabel.setFont(new Font("Times New Roman ", Font.ITALIC, 24));
			lblNewLabel.setBounds(30, 250, 118, 20);
			frame.getContentPane().add(lblNewLabel);

			textTime.setEditable(false);
			textTime.setBounds(170, 250, 50, 20);
			textTime.setFont(new Font("Times New Roman", Font.BOLD, 20));
			textTime.setHorizontalAlignment(SwingConstants.CENTER);
			textTime.setColumns(10);
			frame.getContentPane().add(textTime);

			JScrollPane scrollPane = new JScrollPane();
			scrollPane.setBounds(10, 290, 774, 209);
			scrollPane.setViewportView(logTxt);
			frame.getContentPane().add(scrollPane);

			JButton btnClearLog = new JButton("Refresh");
			btnClearLog.setFont(new Font("Times New Roman", Font.BOLD, 15));
			btnClearLog.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					Scheduler.setLog("");
				}
			});
			btnClearLog.setBounds(695, 250, 89, 23);
			frame.getContentPane().add(btnClearLog);

		}

		
		private void verifyAndCreateQueueManager() {
			// tastari de inputuri gresite
			if (txtMinArrival.getText().isEmpty()
					|| txtMaxArrival.getText().isEmpty()
					|| txtMinServing.getText().isEmpty()
					|| txtMaxServing.getText().isEmpty()
					|| txtQueueNr.getText().isEmpty()
					|| txtSimTime.getText().isEmpty()) {
				JOptionPane.showMessageDialog(null, "Please fill in all the fields");
			} else {
				if (Integer.parseInt(txtMinArrival.getText()) > Integer
						.parseInt(txtSimTime.getText())
						|| Integer.parseInt(txtMaxArrival.getText()) > Integer
								.parseInt(txtSimTime.getText())) {
					JOptionPane.showMessageDialog(null,
							"Arrival time > Simulation time");
				} else {
					if (Integer.parseInt(txtMinServing.getText()) > Integer
							.parseInt(txtSimTime.getText())
							|| Integer.parseInt(txtMaxServing.getText()) > Integer
									.parseInt(txtSimTime.getText())) {
						JOptionPane.showMessageDialog(null,
								"Serving time > Simulation time");
					} else {
						int mSrT = (int) Integer.parseInt(txtMinServing.getText());
						int MSrT = (int) Integer.parseInt(txtMaxServing.getText());
						int mArT = (int) Integer.parseInt(txtMinArrival.getText());
						int MArT = (int) Integer.parseInt(txtMaxArrival.getText());
						int QNr = (int) Integer.parseInt(txtQueueNr.getText());
						int STime = (int) Integer.parseInt(txtSimTime.getText());

					
						Scheduler cons = new Scheduler(mSrT, MSrT, mArT, MArT, QNr,
								STime);
						cons.start();
					}
				}
			}
		}

}

	
