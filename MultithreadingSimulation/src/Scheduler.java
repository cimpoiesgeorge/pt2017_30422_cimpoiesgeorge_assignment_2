

import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

;

public class Scheduler extends Thread {

		private int minServingTime;
		private int maxServingTime;
		private int minArrivalTime;
		private int maxArrivalTime;
		private int nrServers;
		private int simulationTime;
		private int clientCount;
		private static int realTime;
		private static int waitingTime;
		private int avgTime;
		private int emptyTime;
		private int maxClients;
		private int maxRushHour;
	

		//private JFrame graphicPopup;
		private Graphic graphView;

		private ArrayList<Task> Tasks = new ArrayList<>();
		private Random rand = new Random();

		public static Servers[] servers;

		private static String textLog = "";

		public Scheduler(int minServingTime, int maxServingTime,
				int minArrivalTime, int maxArrivalTime, int nrServers, int simulationTime) {

			this.minServingTime = minServingTime;
			this.maxServingTime = maxServingTime;
			this.minArrivalTime = minArrivalTime;
			this.maxArrivalTime = maxArrivalTime;
			this.nrServers = nrServers;
			this.simulationTime = simulationTime;
			this.clientCount = simulationTime/3;

			servers = new Servers[this.nrServers];

			for (int i = 0; i < nrServers; i++) {
				servers[i] = new Servers();
			}
		}

		synchronized public static void setLog(String s) {
			textLog = s;
			GUI.logTxt.setText(textLog);
		}

		// add to Log
		synchronized public static void appendLog(String s) {
			textLog += s;
		}

		// return log
		synchronized public static String getLog() {
			return textLog;
		}

		// timp curent
		public synchronized static int getRealTime() {
			return realTime;
		}

		public static void addWaitingTime(int x) {
			waitingTime = x;
		}

		public static int getWaitingTime() {
			return waitingTime;
		}

		// get optimal queue-> next queue
		public int getNextQueue() {
			int coada = 0;
			if (servers.length > 1) {
				int min = servers[0].size();
				for (int i = 1; i < servers.length; i++) {
					if (servers[i].size() < min) {
						min = servers[i].size();
						coada = i;
					}
				}
			}
			return coada;
		}
		
		
		private void updateRushHour() {
			int clients = 0;
			for (int i = 0; i < servers.length; i++) {
				clients += servers[i].size();
			}
			if (clients > maxClients) {
				maxClients = clients;
				maxRushHour = realTime;
			}
		}
		
		private void updateEmptyTime() {
			for (int i = 0; i < servers.length; i++) {
				emptyTime += servers[i].getWaitingTime();
			}
		}
		

		// update queue in each moment
		public void updateGraphView() {
			graphView.updateThis();
		}

		// settings (simulation)
		private void startSim() {
			for (int i = 0; i < clientCount; i++) {
				int arrTime = minArrivalTime
						+ rand.nextInt(maxArrivalTime - minArrivalTime);
				int servingTime = minServingTime
						+ rand.nextInt(maxServingTime - minServingTime);
				Tasks.add(new Task(servingTime, arrTime, i));
			}


			// creat frame
			JFrame graficPopup = new JFrame();
			graficPopup.setBounds(GUI.getScreenWidth() / 2 + 20, GUI.getScreenHeight() / 2 - 500 / 2, 500, 500);
			graficPopup.setTitle("Simulation");
			graficPopup.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			graficPopup.add(graphView = new Graphic());
			graficPopup.setVisible(true);

			
			// threads for each queue
			for (Servers coada : servers) {
				coada.start();
			}

		}

		// RUN -> main thread
		public void run() {
			startSim();
			for (realTime = 1; realTime < simulationTime; realTime++) {

				GUI.textTime.setText(Integer.toString(realTime));

				for (Task client : Tasks) {
					if (client.getArrivalTime() == realTime) {
						int c = getNextQueue();
						servers[c].addTask(client);
						avgTime += client.getServingTime();

						Scheduler.appendLog("Client   " + (client.getcId() + 1)
								+ "  arrives at moment:   " + client.getArrivalTime()
								+ "      at queue:  " + (c + 1)
								+ "        is served in: "+ (client.getServingTime() - 1) 
								+ "\n");
					}
				}
				updateGraphView();
				updateRushHour();

				try {
					GUI.logTxt.setText(Scheduler.getLog());
					sleep(1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			
			// calculez timpul
			updateEmptyTime();
			
			// Mesajul de final cu datele simularii
			showResults();

		}

		
		
		private void showResults()
		{
			String result = new String("Simulation time: "+ simulationTime 
					+ " seconds" + "\nAverage waiting time: " + ((float) waitingTime / clientCount)
					+ "\nAverage serving time is: " + ((float) avgTime / clientCount)
					+ "\nEmpty queue time is: " + ((float) emptyTime / servers.length)
					+ "\nRush hour: " + maxRushHour);
			
			JOptionPane.showMessageDialog(null, result);
			
			Scheduler.appendLog("\n   ------------------------------   \n\n"+result);
			GUI.logTxt.setText(Scheduler.getLog());
		}

		
		
}